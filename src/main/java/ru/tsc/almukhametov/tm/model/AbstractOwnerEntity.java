package ru.tsc.almukhametov.tm.model;

import lombok.Getter;
import lombok.Setter;

public abstract class AbstractOwnerEntity extends AbstractEntity {

    @Getter
    @Setter
    protected String userId;

}
