package ru.tsc.almukhametov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.api.repository.ITaskRepository;
import ru.tsc.almukhametov.tm.api.service.ITaskService;
import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.exception.empty.*;
import ru.tsc.almukhametov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.almukhametov.tm.exception.system.AccessDeniedException;
import ru.tsc.almukhametov.tm.model.Task;

import java.util.Optional;

public final class TaskService extends AbstractOwnerService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(userId, task);
    }

    @NotNull
    @Override
    public Optional<Task> findByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByName(userId, name);
    }

    @NotNull
    @Override
    public Optional<Task> removeByName(@Nullable final String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.removeByName(userId, name);
    }

    @NotNull
    @Override
    public Task updateById(@Nullable final String userId, final String id, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final Optional<Task> task = taskRepository.findById(userId, id);
        task.get().setName(name);
        task.get().setDescription(description);
        return task.get();
    }

    @NotNull
    @Override
    public Task updateByIndex(@Nullable final String userId, final Integer index, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new EmptyIdException();
        if (taskRepository.getSize() < index - 1) throw new TaskNotFoundException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Optional<Task> task = taskRepository.findByIndex(userId, index);
        task.get().setName(name);
        task.get().setDescription(description);
        return task.get();
    }

    @NotNull
    @Override
    public Task startById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final Optional<Task> task = taskRepository.findById(userId, id);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.get().setStatus(Status.IN_PROGRESS);
        return task.get();
    }

    @NotNull
    @Override
    public Task startByIndex(@Nullable final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (taskRepository.getSize() < index - 1) throw new TaskNotFoundException();
        final Optional<Task> task = taskRepository.findByIndex(userId, index);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.get().setStatus(Status.IN_PROGRESS);
        return task.get();
    }

    @NotNull
    @Override
    public Task startByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Optional<Task> task = taskRepository.findByName(userId, name);
        if (!task.isPresent()) return null;
        task.get().setStatus(Status.IN_PROGRESS);
        return task.get();
    }

    @NotNull
    @Override
    public Task finishById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Optional<Task> task = taskRepository.findById(userId, id);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.get().setStatus(Status.COMPLETED);
        return task.get();
    }

    @NotNull
    @Override
    public Task finishByIndex(@Nullable final String userId, final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (taskRepository.getSize() < index - 1) throw new TaskNotFoundException();
        final Optional<Task> task = taskRepository.findByIndex(userId, index);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.get().setStatus(Status.COMPLETED);
        return task.get();
    }

    @NotNull
    @Override
    public Task finishByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Optional<Task> task = taskRepository.findById(userId, name);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.get().setStatus(Status.COMPLETED);
        return task.get();
    }

    @NotNull
    @Override
    public Task changeTaskStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        return taskRepository.changeTaskStatusById(userId, id, status);
    }

    @NotNull
    @Override
    public Task changeTaskStatusByIndex(@Nullable final String userId, final Integer index, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (taskRepository.getSize() < index - 1) throw new TaskNotFoundException();
        if (status == null) throw new EmptyStatusException();
        return taskRepository.changeTaskStatusByIndex(userId, index, status);
    }

    @NotNull
    @Override
    public Task changeTaskStatusByName(@Nullable final String userId, @Nullable final String name, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        return taskRepository.changeTaskStatusByName(userId, name, status);
    }

}
