package ru.tsc.almukhametov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.api.IRepository;
import ru.tsc.almukhametov.tm.api.IService;
import ru.tsc.almukhametov.tm.exception.empty.EmptyIdException;
import ru.tsc.almukhametov.tm.exception.empty.EmptyIndexException;
import ru.tsc.almukhametov.tm.exception.entity.EntityNotFoundException;
import ru.tsc.almukhametov.tm.model.AbstractEntity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    protected IRepository<E> repository;

    public AbstractService(final IRepository<E> repository) {
        this.repository = repository;
    }

    @Nullable
    @Override
    public E add(@Nullable final E entity) {
        if (entity == null) throw new EntityNotFoundException();
        return repository.add(entity);
    }

    @Override
    public void addAll(@Nullable List<E> entities) {
        if (entities == null) throw new EntityNotFoundException();
        repository.addAll(entities);
    }

    @Nullable
    @Override
    public Optional<E> remove(@Nullable E entity) {
        if (entity == null) throw new EntityNotFoundException();
        repository.remove(entity);
        return Optional.empty();
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable Comparator<E> comparator) {
        if (comparator == null) return Collections.emptyList();
        return repository.findAll(comparator);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Nullable
    @Override
    public Optional<E> findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Nullable
    @Override
    public Optional<E> findByIndex(final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (repository.getSize() < index - 1) throw new EntityNotFoundException();
        return repository.findByIndex(index);
    }

    @Nullable
    @Override
    public Optional<E> removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(id);
    }

    @Nullable
    @Override
    public Optional<E> removeByIndex(final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (repository.getSize() < index - 1) throw new EntityNotFoundException();
        return repository.removeByIndex(index);
    }


    @Override
    public boolean existById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.existById(id);
    }

    @Override
    public boolean existByIndex(final int index) {
        return repository.existByIndex(index);
    }

    @Override
    public Integer getSize() {
        return repository.getSize();
    }

}
