package ru.tsc.almukhametov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.model.Project;

import java.util.Optional;

public interface IProjectService extends IOwnerService<Project> {

    void create(@NotNull final String userId, @NotNull final String name, @NotNull final String description);

    @NotNull
    Optional<Project> findByName(@Nullable final String userId, @Nullable final String name);

    @NotNull
    Optional<Project> removeByName(@Nullable final String userId, @Nullable final String name);

    @NotNull
    Project updateById(@NotNull final String userId, @NotNull final String id, @NotNull final String name, @NotNull final String description);

    @NotNull
    Project updateByIndex(@Nullable final String userId, final Integer index, @Nullable final String name, @Nullable final String description);

    @NotNull
    Project startById(@Nullable final String userId, @Nullable final String id);

    @NotNull
    Project startByIndex(@Nullable final String userId, final Integer index);

    @NotNull
    Project startByName(@Nullable final String userId, @Nullable final String name);

    @NotNull
    Project finishById(@Nullable final String userId, @Nullable final String id);

    @NotNull
    Project finishByIndex(@Nullable final String userId, final Integer index);

    @NotNull
    Project finishByName(@Nullable final String userId, @Nullable final String name);

    @NotNull
    Project changeProjectStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status);

    @NotNull
    Project changeProjectStatusByIndex(@Nullable final String userId, final Integer index, @Nullable final Status status);

    @NotNull
    Project changeProjectStatusByName(@Nullable final String userId, @Nullable final String name, @Nullable final Status status);

}
