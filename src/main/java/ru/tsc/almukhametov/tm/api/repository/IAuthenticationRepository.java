package ru.tsc.almukhametov.tm.api.repository;

import org.jetbrains.annotations.Nullable;

public interface IAuthenticationRepository {

    @Nullable
    String getCurrentUserId();

    void setCurrentUserId(@Nullable String currentUserId);

}
