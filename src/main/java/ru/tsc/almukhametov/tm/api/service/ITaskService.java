package ru.tsc.almukhametov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.enumerated.Status;
import ru.tsc.almukhametov.tm.model.Task;

import java.util.Optional;

public interface ITaskService extends IOwnerService<Task> {

    void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description);

    @NotNull
    Optional<Task> findByName(@Nullable final String userId, @Nullable final String name);

    @NotNull
    Optional<Task> removeByName(@Nullable final String userId, @Nullable final String name);

    @NotNull
    Task updateById(@Nullable final String userId, final String id, @Nullable final String name, @Nullable final String description);

    @NotNull
    Task updateByIndex(@Nullable final String userId, final Integer index, @Nullable final String name, @Nullable final String description);

    @NotNull
    Task startById(@Nullable final String userId, @Nullable final String id);

    @NotNull
    Task startByIndex(@Nullable final String userId, @Nullable final Integer id);

    @NotNull
    Task startByName(@Nullable final String userId, @Nullable final String name);

    @NotNull
    Task finishById(@Nullable final String userId, @Nullable final String id);

    @NotNull
    Task finishByIndex(@Nullable final String userId, final Integer index);

    @NotNull
    Task finishByName(@Nullable final String userId, @Nullable final String name);

    @NotNull
    Task changeTaskStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status);

    @NotNull
    Task changeTaskStatusByIndex(@Nullable final String userId, final Integer index, @Nullable final Status status);

    @NotNull
    Task changeTaskStatusByName(@Nullable final String userId, @Nullable final String name, @Nullable final Status status);

}
