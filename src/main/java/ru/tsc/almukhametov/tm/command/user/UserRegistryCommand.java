package ru.tsc.almukhametov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.command.AbstractUserCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;
import ru.tsc.almukhametov.tm.enumerated.Role;
import ru.tsc.almukhametov.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String name() {
        return TerminalConst.USER_CREATE;
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return SystemDescriptionConst.USER_CREATE;
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRY]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER E-MAIL:");
        @Nullable final String email = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthenticationService().registry(login, password, email);
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
