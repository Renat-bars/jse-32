package ru.tsc.almukhametov.tm.command.union;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.almukhametov.tm.command.AbstractProjectTaskCommand;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;

public final class ProjectClearTaskCommand extends AbstractProjectTaskCommand {

    @NotNull
    @Override
    public String name() {
        return TerminalConst.PROJECT_CLEAR;
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return SystemDescriptionConst.PROJECT_CLEAR;
    }

    @Override
    public void execute() {
        @NotNull final String userId = String.valueOf(serviceLocator.getAuthenticationService().getCurrentUserId());
        System.out.println("[ClEAR PROJECTS]");
        serviceLocator.getProjectTaskService().clearProjects(userId);
        System.out.println("[SUCCESS CLEAR]");
    }

}
