package ru.tsc.almukhametov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.almukhametov.tm.command.AbstractCommand;
import ru.tsc.almukhametov.tm.constant.ArgumentConst;
import ru.tsc.almukhametov.tm.constant.SystemDescriptionConst;
import ru.tsc.almukhametov.tm.constant.TerminalConst;

import java.util.Collection;

public final class Commands extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return TerminalConst.COMMANDS;
    }

    @NotNull
    @Override
    public String arg() {
        return ArgumentConst.COMMANDS;
    }

    @NotNull
    @Override
    public String description() {
        return SystemDescriptionConst.COMMANDS;
    }

    @Override
    public void execute() {
        @NotNull final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (final AbstractCommand command : commands) {
            System.out.println(command.name());
        }
    }

}
