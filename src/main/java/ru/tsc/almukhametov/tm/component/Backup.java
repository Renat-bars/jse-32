package ru.tsc.almukhametov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static ru.tsc.almukhametov.tm.constant.TerminalConst.BACKUP_LOAD_XML;
import static ru.tsc.almukhametov.tm.constant.TerminalConst.BACKUP_SAVE_XML;

public class Backup {

    @NotNull
    public final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @SneakyThrows
    public void start() {
        final Long delay = bootstrap.getPropertyService().getBackupInterval();
        executorService.scheduleWithFixedDelay(this::save, delay, delay, TimeUnit.SECONDS);
    }

    public void init() {
        load();
        start();
    }

    public void load() {
        bootstrap.parseCommands(BACKUP_LOAD_XML);
    }

    public void save() {
        bootstrap.parseCommands(BACKUP_SAVE_XML);
    }

}
