package ru.tsc.almukhametov.tm.exception.empty;

import ru.tsc.almukhametov.tm.exception.AbstractException;

public class EmptyDomainException extends AbstractException {

    public EmptyDomainException() {
        super("Error, domain is empty");
    }
}
