package ru.tsc.almukhametov.tm.exception.empty;

import ru.tsc.almukhametov.tm.exception.AbstractException;

public class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("Error, password is empty");
    }
}
