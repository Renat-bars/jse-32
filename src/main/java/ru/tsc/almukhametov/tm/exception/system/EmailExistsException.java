package ru.tsc.almukhametov.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.almukhametov.tm.constant.ArgumentConst;
import ru.tsc.almukhametov.tm.exception.AbstractException;

public class EmailExistsException extends AbstractException {

    public EmailExistsException(@NotNull final String email) {
        super("Incorrect argument. Argument ``" + email + "`` was not founded. " +
                "Use ``" + ArgumentConst.HELP + "`` for display list of arguments");
    }
}
