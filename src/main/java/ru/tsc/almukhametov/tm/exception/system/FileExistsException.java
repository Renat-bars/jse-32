package ru.tsc.almukhametov.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.almukhametov.tm.exception.AbstractException;

public class FileExistsException extends AbstractException {

    public FileExistsException(@NotNull final String file) {
        super("Error! File" + file + " was not founded.");
    }
}
