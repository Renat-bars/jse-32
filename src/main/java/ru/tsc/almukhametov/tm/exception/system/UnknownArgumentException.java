package ru.tsc.almukhametov.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.almukhametov.tm.constant.ArgumentConst;
import ru.tsc.almukhametov.tm.exception.AbstractException;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException(@NotNull final String arg) {
        super("Incorrect argument. Argument ``" + arg + "`` was not founded. " +
                "Use ``" + ArgumentConst.HELP + "`` for display list of arguments");
    }

}
